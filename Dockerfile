FROM node:14

LABEL org.opencontainers.image.source="https://github.com/metalstormbass/goof" 
LABEL io.snyk.containers.image.dockerfile="/Dockerfile"

RUN mkdir /usr/src/goof
RUN mkdir /tmp/extracted_files
COPY . /usr/src/goof
WORKDIR /usr/src/goof

RUN npm update
RUN npm install

RUN apt-get update
RUN apt install sa-exim -y
RUN apt-get install iputils-ping -y
RUN apt-get install nmap -y

EXPOSE 3001
EXPOSE 9229
ENTRYPOINT ["npm", "start"]
